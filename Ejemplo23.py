#EP23. La empresa "Compre Aquí" desea estimular a sus empleados, haciéndoles un
#aumento de salario, que va de acuerdo a la categoría del empleado de la siguiente manera:
#-Si la categoría es 1 el aumento es del 15% sobre el salario actual.
#-Si la categoría es 2 el aumento es del 20% sobre el salario actual.
#-Si la categoría es 3 el aumento es del 25% sobre el salario actual.
#Diseñe un algoritmo que reciba como entrada la categoría y el salario actual del trabajador,
#calcule e imprima el nuevo salario.

Categoria=int(input("Ingrese la categoria : "))
Salario=int(input("Ingrese el salario actual : "))
Categoria==1 or 2 or 3
C1=Salario*0.15
C2=Salario*0.20
C3=Salario*0.25

SalarioTotal1=C1+Salario
SalarioTotal2=C2+Salario
SalarioTotal3=C3+Salario
if Categoria==1:
    print("El aumento del 15% es de : ",C1, " y el Salario Total con el aumento es de : ",SalarioTotal1)
elif Categoria==2:
    print("El aumento del 20% es de : ",C2, " y el Salario Total con el aumento es de : ",SalarioTotal2)
else:
    print("El aumento del 25% es de : ",C3, " y el Salario Total con el aumento es de : ",SalarioTotal3)



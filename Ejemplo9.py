#EP9. Realice un algoritmo que determine el pago a realizar por la entrada a un
#espectáculo donde se pueden comprar sólo hasta cuatro entrada, donde al costo de dos
#entradas se les descuenta el 10%, al de tres entrada el 15% y a la compra de cuatro
#tickets se le descuenta el 20 %

CantidadEntradas=int(input("Ingrese la cantidad de entradas : "))

precioBase = 10000
descuento2=((precioBase*CantidadEntradas)*0.10)
descuento3=((precioBase*CantidadEntradas)*0.15)
descuento4=((precioBase*CantidadEntradas)*0.20)

if CantidadEntradas==1:
    print("Debe pagar : " ,precioBase)

elif (CantidadEntradas==2):
    brutoPagar=(precioBase*CantidadEntradas)
    montoPagar=brutoPagar-descuento2
    print("Debe pagar : ",brutoPagar, " su descuento es de : ",descuento2, " su neto a pagar es : ",montoPagar)

elif CantidadEntradas==3:
    brutoPagar=(precioBase*CantidadEntradas)
    montoPagar=brutoPagar-descuento3
    print("Debe pagar : ",brutoPagar," su descuento es de : ",descuento3, " su neto a pagar es : ",montoPagar)

elif CantidadEntradas==4:
    brutoPagar=(precioBase*CantidadEntradas)
    montoPagar=brutoPagar-descuento4
    print("Debe pagar : ",brutoPagar," su descuento es de :",descuento4, " su neto a pagar es : ",montoPagar)

else:
    print("El maximo de entradas a comprar son 4")

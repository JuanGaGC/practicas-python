#EP2. Realizar el diagrama de flujo para que nos calcule la hipotenusa de un triángulo
#rectángulo, conocidos su dos catetos.

import math

cateto1=int(input("Ingrese cateto 1 : "))
cateto2=int(input("Ingrese cateto 2 : "))

hipotenusa=math.sqrt((pow(cateto1,2)+pow(cateto2,2)))

print("La hipotenusa es : ",hipotenusa)

#EP22. Un conleccionista y vendedor de carros antiguos desea imprimir a un cliente la lista
#de los carros que tiene su empresa y que cumplan con la condición que dicho cliente pide.
#Autos con número de placa entre 5000 y 8000, y que sean modelo 1958 o 1959.
#Diseñe un algoritmo que reciba como entrada el número de placa y el año de un auto e
#imprima si el vehículo podría o no interesarle al cliente.

Placa=int(input("Ingrese el numero de placa : "))
Modelo=int(input("Ingrese el modelo : "))

if ((Placa>4999) and (Placa<8001)) and ((Modelo==1958) or (Modelo==1959)):
    print("Al cliente le interesa este vehiculo")
else:
    print("Al cliente no le interesa este vehiculo")

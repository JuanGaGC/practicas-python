#Ejemplo para que el texto quede centrado y entre (===...===)
MensajeEntrada="programa de generacion de tablas de multiplicar".capitalize()
print(MensajeEntrada.center(60,"="))


#Ejemplo para digitar letras o numeros
#Opcion=1
Opcion ='S'

#Ejemplo para salir con numeros
#while Opcion!=0:

#Ejemplo para salir con letras
while (Opcion!='N') or Opcion!= 'n':
    tabInicio=int(input(chr(27) + "[1;32m" + "Ingrese la tabla inicial : "))
    tabFinal=int(input("Ingrese la tabla final : "))

    #Ejemplo para entrar al proceso hasta que se ejecute correctamente
    while tabFinal<tabInicio:
        #Ejemplo para cambiar el color de las letras
        print(chr(27) + "[1;35m" + "La tabla de inicio debe ser menor ")
        tabInicio=int(input("Ingrese la tabla inicial : "))
        tabFinal=int(input("Ingrese la tabla final : "))
    #Las variables tienen que quedar en linea con el siguiente while para que las reconoza
    rangoInicio=int(input(chr(27) + "[1;32m" + "Ingrese el rango inicial : "))
    rangoFinal=int(input("Ingrese el rango final : "))

    #Ejemplo para entrar al proceso hasta que se ejecute correctamente
    while rangoFinal<rangoInicio:
        print(chr(27) + "[1;35m" + "El rango de inicio debe ser menor ")
        rangoInicio=int(input("Ingrese el rango inicial : "))
        rangoFinal=int(input("Ingrese el rango final : "))

    while tabInicio<tabFinal:
        #El +1 es para que empiece del 1 y no del 0
        for i in range(tabInicio,tabFinal+1):
            #Ejemplo para omitir una linea deseada en el resultado
            if i==4:
                print(chr(27) + "[1;31m" + "La tabla (0) no la imprimo porque no quiero ".format(i))
                continue
            #Ejemplo para que el resultado llegue hasta hasta el punto deseado
            for j in range(rangoInicio,rangoFinal+1):
                if j==5:
                    print(chr(27) + "[1;31m" + "Mas de 5 no quiero")
                    break

                Resultado=i*j
                #Ejemplos de digitar la manera de imprimir el resultado final
                #print("Multiplicar ",i, " * ",j, " es igual a : ",Resultado)
                print(chr(27) + "[1;32m" + "Multiplicar %d * %d es igual a : %d "%(i,j,Resultado))

        tabInicio=tabInicio+1
    #Ejemplo para escoger la opcion entre salir o continuar el proceso
    Opcion=input(chr(27) + "[1;35m" + "Desea ejecutar nuevamente el proceso : \n"
                     "N: Salir\n"
                     "S: Continuar \n")

    #Ejemplo para que cuando se digite dichas letras, el proceso pare
    if (Opcion=='n') or (Opcion=='N'):
        break

else:
    #Ejemplo para cuando el proceso termine digite dicho texto
    print("Gracias por jugar a multiplicar ")

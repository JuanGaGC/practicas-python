#EP12. Cree un diagrama de flujo que dado un año A indique si es o no bisiesto. Un año es
#bisiesto si es múltiplo de 4, exceptuando los múltiplos de 100, que sólo son bisiestos
#cuando son múltiplos de 400, por ejemplo el año 1900 no fue bisiesto, pero el año
#2000 si lo fue.

Anno=int(input("Digite el año")

if Anno MOD 4==0:
    print("Es año bisiesto")
elif Anno MOD 400==0:
    print("Es año bisiesto")
else:
    print("No es año bisiesto")

#EP19. Diseñe un algoritmo que reciba el largo y el ancho de un terreno y calcule el área;
#si dicha área es mayor a 100 m2, entonces el sistema deberá imprimir como salida "Terreno apto
#para construcción", en caso contrario se imprimiría "Terreno no apto para construcción".

Largo=int(input("Ingrese el largo del terreno : "))
Ancho=int(input("Ingrese el ancho del terreno : "))

Area=Largo*Ancho

if Area>100:
    print("Terreno apto para construccion")
else:
    print("Terreno no apto para construccion")


#EP8. Cree un diagrama de flujo que permita leer tres valores y almacenarlos en las
#variables A, B y C respectivamente. El algoritmo debe imprimir cual es el mayor y cual es
#el menor. Recuerde constatar que los tres valores introducidos por el teclado sean
#valores distintos. Presente un mensaje de error en caso de que se detecte la
#introducción de valores iguales, y proceda a terminar la ejecución del mismo.

valor1=int(input("Ingrese el primer valor : "))
valor2=int(input("Ingrese el segundo valor : "))
valor3=int(input("Ingrese el tercer valor : "))

if ((valor1<valor2) and (valor2<valor3)):
    print("El primer valor es menor y el tercer valor es mayor")
elif ((valor1<valor2) and (valor2==valor3)):
    print("Error, el segundo valor es igual al tercer valor")
elif (((valor1<valor2 and valor3)) and (valor2==valor3)):
    print("Error, los valores 2 y 3 son iguales")
elif ((valor2<valor1) and (valor1<valor3)):
    print("El segundo valor es menor y el tercer valor es mayor")
elif ((valor2<valor1) and (valor1==valor3)):
    print("Error, el primer valor es igual al tercer valor" )
elif (((valor2<valor1 and valor3)) and (valor1==valor3)):
    print("Error, los valores 1 y 3 son iguales")
elif ((valor3<valor1) and (valor1<valor2)):
    print("El tercer valor es menor y el segundo valor es mayor")
elif ((valor3<valor1) and (valor1==valor2)):
    print("Error, el primer valor es igual al segundo valor")
elif (((valor3<valor2 and valor1)) and (valor1==valor2)):
    print("Error, los valores 1 y 2 son iguales")
elif (valor1==valor2 and valor3):
    print("Error, todos los valores son iguales")
else
    print("Todos los valores son diferentes")


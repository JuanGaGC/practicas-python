#EP16. Diseñe un algoritmo que reciba un número entero, si dicho número es positivo, entonces se debe imprimir la
#raíz cuadrada del mismo, si el número es negativo se debe imprimir el cuadrado y el cubo.

import math
A=int(input("Digite el numero entero : "))

if A>0:
    print("La raiz cuadrada de ",A, "es : ",math.sqrt(A))
else:
    print("El cuadrado de ", str(-A), "es : ",str(-A*-A), "y el cubo es : ",str(-A*-A*-A))


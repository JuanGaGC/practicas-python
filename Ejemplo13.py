#EP13. Para ingresar a un curso de alto nivel, los candidatos deben realizar tres exámenes,
#A, B, C. Diseñe un algoritmo que reciba como entrada la edad y las notas de los tres
#exámenes. Calcule y despliegue el promedio de notas obtenido y un mensaje indicando
#si fue aceptado o no en el curso sabiendo que:
#– Tener entre 15 y 18 años inclusive y obtener promedio de los exámenes mayor a 90.
#– Tener más de 18 años y que su promedio esté entre 80 y 90 inclusive.
#– Tener menos de 15 años y un promedio mayor o igual a 90, o un promedio mayor o
#igual a 80 pero no debe obtener menos de un 85 en la nota del examen c.

Edad=int(input("Ingrese la edad del estudiante : "))
Nota1=int(input("Ingrese la primer nota : "))
Nota2=int(input("Ingrese la segunda nota : "))
Nota3=int(input("Ingrese la tercer nota : "))

Promedio=(Nota1+Nota2+Nota3)/3

if Edad > 15 and Edad < 18 and Promedio > 90:
    print("El estudiante fue aprobado en el curso")
elif Edad > 18 and Promedio >= 80 and Promedio <= 90:
    print("El estudiante fue aprobado en el curso")
elif Edad < 15 and Promedio >= 90:
    print("El estudiante fue aprobado en el curso")
elif Promedio >= 80 and Nota3 >= 85:
    print("El estudiante fue aprobado en el curso")
else:
    print("El estudiante no fue aprobado en el curso")
